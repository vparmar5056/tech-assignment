<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\ApiController;
use App\Models\Client;
use App\Models\Project;
use App\Models\Cost;
use App\Models\Cost_type;
use DB;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class ClientController extends ApiController
{
    /**
     * @param Request $request
     * @return \App\Http\Controllers\json
     */
    public function index(Request $request)
    {
         $data      = array();
         $clients   = Client::query();

         if(request()->has('client_id')) {
             $clients->whereIn('id', $request->input('client_id'));
         }

         $records = $clients->get()->toArray();

        $client_cost = 0;        
        foreach($records as $key => $client)
        {
            $projects_query   = Project::query();
            if(request()->has('project_id')) {
                $projects_query->whereIn('id', $request->input('project_id'));
            }

            $projects = $projects_query
            ->where('client_id', '=',$client['id'])
                ->orderBy('id', 'ASC')
                ->get()->toArray();

            $project_data = array();
            foreach($projects as $project)
            {
                $cost_type_data = array();

                $cost_type_query   = Cost_type::query()
                ->select('cost_types.id','cost_types.name','costs.amount')
                ->join('costs', 'costs.cost_type_id', '=', 'cost_types.id');

                 if(request()->has('cost_type_id') && request()->has('project_id')) {
                    $cost_type_query->WhereIn('cost_types.id', $request->input('cost_type_id'))
                    ->WhereIn('project_id', $request->input('project_id'));
                }else
                {
                    $cost_type_query->where('parent_id', '=', Null)
                    ->where('project_id', '=', $project['id']);
                }

                $cost_types = $cost_type_query
                    ->orderBy('cost_types.id', 'ASC')
                    ->get()->toArray();

                    $project_cost = 0;
                foreach($cost_types as $cost_type)
                {
                            $cost_type_parent_data = array();

                            $cost_type_parent_query   = Cost_type::query()
                            ->select('cost_types.id','cost_types.name','costs.amount')
                            ->join('costs', 'costs.cost_type_id', '=', 'cost_types.id')
                            
                            ->where('project_id', '=', $project['id']);

                            if(request()->has('cost_type_id') && request()->has('project_id')) {
                                $cost_type_parent_query->whereIn('cost_type_id', $request->input('cost_type_id'))
                                ->whereIn('project_id', $request->input('project_id'));
                            }
                            elseif(request()->has('cost_type_id')) {
                                $rs =   Cost_type::select('parent_id')
                                ->whereIn('id', $request->input('cost_type_id'))
                                ->get()->toArray();
                                $cost_type_parent_query->whereIn('cost_types.id', $rs);
                            }else
                            {
                                $cost_type_parent_query->where('parent_id', '=', $cost_type['id']);
                            }
                            $cost_type_parents = $cost_type_parent_query
                                ->orderBy('cost_types.id', 'ASC')
                                ->get()->toArray();

                        foreach($cost_type_parents as $cost_type_parent)
                        {
                            $cost_type_child_data = array();
                           
                            $cost_type_child_query   = Cost_type::query()
                            ->select('cost_types.id','cost_types.name','costs.amount')
                            ->join('costs', 'costs.cost_type_id', '=', 'cost_types.id')
                            ->where('project_id', '=', $project['id']);
                
                            if(request()->has('cost_type_id')) {
                                $cost_type_child_query->whereIn('cost_types.id', $request->input('cost_type_id'));
                            }else
                            {
                                $cost_type_child_query->where('parent_id', '=', $cost_type_parent['id']);
                            }
                            $cost_type_childs = $cost_type_child_query
                                ->orderBy('cost_types.id', 'ASC')
                                ->get()->toArray();

                                foreach($cost_type_childs as $cost_type_child)
                            {
                                $cost_type_child_data[] = [
                                    "id"        =>  $cost_type_child['id'],
                                    "type"      => "cost",
                                    "name"      => $cost_type_child['name'],
                                    "amount"    => number_format((float)$cost_type_child['amount'], 2, '.', ''),
                                    "children"  => []
                                ];
                            }

                            $cost_type_parent_data[] = [
                                "id"        => $cost_type_parent['id'],
                                "type"      => "cost",
                                "name"      => $cost_type_parent['name'],
                                "amount"    => number_format((float)$cost_type_parent['amount'], 2, '.', ''),
                                "children"  => $cost_type_child_data,
                            ];
                        }
                    $cost_type_data[] = [
                        "id"        => $cost_type['id'],
                        "type"      => "cost",
                        "name"      => $cost_type['name'],
                        "amount"    => number_format((float)$cost_type['amount'], 2, '.', ''),
                        "children"  => $cost_type_parent_data,
                    ];

                    $project_cost += $cost_type['amount'];
                }
                $project_data[] = [
                    "id"        => $project['id'],
                    "name"      => $project['title'],
                    "type"      => "project",
                    "amount"    => $project_cost,
                    "children"  => $cost_type_data,
                ];
                $client_cost += $project_cost;
            }
            
             $data[] = [
                "id"        => $client['id'],
                "name"      => $client['name'],
                "type"      => "client",
                "amount"    => $client_cost,
                "children"  => $project_data,
             ];
        }

        $this->apiResponse['data'] = $data;
        return $this->sendResponse();
    }
}
