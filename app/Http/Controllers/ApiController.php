<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * API response code
     * @var int
     */
    private $responseCode = 200;

    /**
     * This variable hold the api response object
     * @var array
     */
    protected $apiResponse = array(
        'response_code' => 200,
        'data' => [],
        'message' => '',
        'status' => 'ok'
    );

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @param int $responseCode
     */
    public function setResponseCode(int $responseCode): void
    {
        $this->responseCode = $responseCode;
    }

    /**
     * @return array
     */
    public function getApiResponse(): array
    {
        return $this->apiResponse;
    }

    /**
     * @param array $apiResponse
     */
    public function setApiResponse(array $apiResponse): void
    {
        $this->apiResponse = $apiResponse;
    }

    /**
     *
     * @return json output
     */
    protected function sendResponse()
    {
        $this->apiResponse['response_code'] = $this->responseCode;
        return response()->json($this->apiResponse, $this->responseCode);
    }
}
