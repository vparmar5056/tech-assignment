### How to use ###

* Clone the repository with git clone
* git clone https://vparmar5056@bitbucket.org/vparmar5056/tech-assignment.git
* Run composer install
* Run php artisan serve

### Follwing API End Point ###

* http://127.0.0.1:8000/explorer
* http://127.0.0.1:8000/explorer?cost_type_id[]=1&cost_type_id[]=10
* http://127.0.0.1:8000/explorer?cost_type_id[]=7&project_id[]=32&project_id[]=16
